---
title : Photolangage
summary: Atelier de photolange échanger autour de concepts du numérique et du libre 
tags: Littératie du numérique
---

## Internet

Internet, c'est le réseau des réseaux, c'est à dire la mise en réseau mondiale de l'ensemble des réseaux informatiques.

Si on met en relation le réseau informatique d'une université, avec le réseau informatique d'un Fournisseur d'Accès à Internet, avec le réseau auto-géré d'une commune, etc. on crée un bout d'Internet.

Pour communiquer ensemble, les ordinateurs de ces réseaux doivent parler la même langue : ils utilisent des protocoles informatiques. Le web, par exemple, désigne une partie d'Internet qui fonctionne par hyperlien, avec le protocole HTTP (ou HTTPS).

Mais l'email, par exemple, utilise d'autres protocoles : les emails passent par Internet mais sont distincts du Web.

_Source : [Glossaire du MOOC Chatons](https://mooc.chatons.org/mod/glossary/view.php?id=29&mode=letter&hook=I)_

### Légende de l'image

Carte partielle d'Internet, basée sur les données de [opte.org](http://www.opte.org/maps/) du 15 juin 2005. Chaque ligne relie 2 nœuds, représentant 2 [adresses IP](https://fr.wikipedia.org/wiki/Adresse_IP "fr:Adresse IP"). La longueur de chaque ligne donne une indication du délai entre les 2 nœuds. Moins de 30 % des réseaux de [classe C](https://fr.wikipedia.org/wiki/Classe_d%27adresse_IP "fr:Classe d'adresse IP") accessibles par le programme de collecte de données début 2005 sont représentés. Les lignes sont coloriées selon le code de couleur défini par la [RFC 1918](https://tools.ietf.org/html/rfc1918) :

* bleu nuit : net, ca, us ;
* vert : com, org ;
* rouge : mil, gov, edu ;
* jaune : jp, cn, tw, au, de ;
* magenta : uk, it, pl, fr ;
* or : br, kr, nl ;
* blanc : inconnu.

\newpage

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Internet_map_1024.jpg/768px-Internet_map_1024.jpg)

_Source : The Opte Project, CC BY 2.5 <https://creativecommons.org/licenses/by/2.5>, via Wikimedia Commons_

\newpage

## Datacenter
Un **centre de données** (en anglais data center ou data centre) est un lieu (et un service) regroupant des équipements constituants du système d'information d'une ou plusieurs organisations. Il peut être interne et/ou externe à l’organisation, exploité ou non avec le soutien de prestataires. 

Il fournit des services informatiques en environnement contrôlé (climatisation) et sécurité (système anti-incendie, contre le vol et l'intrusion, etc.), avec une alimentation d'urgence et redondante.

Des enjeux environnementaux sont liés d'une part à leur consommation de métaux rares ou précieux et de terres rares, et d'autre part à une consommation croissante d'électricité de l'ensemble des centres de données, et à leur coproduit qu'est la chaleur de récupération, dissipée par les serveurs et les systèmes de stockage en particulier, mais qui peut être une énergie de récupération.

Les centres de données sont responsables de 0,3 % des émissions mondiales de gaz à effet de serre. Les centres informatiques représentent moins de 15 % de l'impact environnemental du numérique à l'échelle mondiale, tandis que les équipements utilisateurs en concentrent les deux tiers, et le réseau la part restante. Rappelons que, selon l'association française The Shift Project, le numérique dans son ensemble était responsable de 3,7 % des émissions de CO2 mondiales en 2018, contre 2,5 % en 2013.

_Source: [Wikipedia - Centre de données](https://fr.wikipedia.org/wiki/Centre_de_données) CC-By-SA-3.0_

\newpage

![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/Wikimedia_Servers-0051_20.jpg/1024px-Wikimedia_Servers-0051_20.jpg)

_Source : Helpameout, CC BY-SA 3.0 <https://creativecommons.org/licenses/by-sa/3.0>, via Wikimedia Commons_

\newpage

## Serveur

Un serveur informatique est un dispositif informatique (matériel et logiciel) qui offre des services à un ou plusieurs clients (parfois des milliers). Les services les plus courants sont :

l'accès aux informations du World Wide Web ;
le courrier électronique ;
    le partage de périphériques (imprimantes, disque durs, etc.) ;
    le commerce électronique ;
    le stockage en base de données ;
    la gestion de l'authentification et du contrôle d'accès ;
    le jeu et la mise à disposition de logiciels applicatifs.
 
_Source : [Wikipedia - Serveur informatique](https://fr.wikipedia.org/wiki/Serveur_informatique) CC-By-SA-3.0_

\newpage

![Diagramme accès internet](img/Internet_access_diagram.svg)

\newpage

## Fournisseur d'accès

Organisme qui offre une connexion, une porte d'entrée à Internet, le réseau des réseaux numériques.

Souvent désigné par l’acronyme F.A.I. , il s'agit d'un des opérateurs du réseau Internet. Les F.A.I. sont souvent des entreprises (en France : Orange, Free, SFR, Bouygues Télécom), mais peuvent aussi être des associations.

En France, la Fédération FDN regroupe des Fournisseurs d'Accès à Internet associatifs se reconnaissant dans des valeurs communes : bénévolat, solidarité, fonctionnement démocratique et à but non lucratif ; défense et promotion de la neutralité du Net.

_Source : [MOOC Chatons](https://mooc.chatons.org/mod/glossary/view.php?id=29&mode=letter&hook=F)_

\newpage

\newpage

## Terres rares

\newpage

![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Rareearthoxides.jpg/640px-Rareearthoxides.jpg)

_Source : Peggy Greb, US department of agriculture, Public domain, via Wikimedia Commons_

\newpage

## Surveillance

\newpage

\newpage

![Privacy, by Zabou street Art](https://zabou.me/wp-content/uploads/2014/02/Privacy-Zabou1.jpg)

_Source : [Zabou](https://zabou.me), artiste street Art_

\newpage

![Reconnaissance faciale]

_Source: [Peter Merholz](https://www.flickr.com/photos/peterme/7645323/) Self-portrait through surveillance technology - CC-By-SA_

## Donnée personnelle

Toute information identifiant directement ou indirectement une personne physique (ex. nom, no d’immatriculation, no de téléphone, photographie, date de naissance, commune de résidence, empreinte digitale...).

_Source : [CNIL](https://www.cnil.fr/fr/definition/donnee-personnelle)_

\newpage

\newpage

## Neutralité du net

\newpage

\newpage

## Domaine public

\newpage

![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/The_Scream.jpg/603px-The_Scream.jpg)

_Source :  [Edvard Munch](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/The_Scream.jpg/603px-The_Scream.jpg), Public domain, via Wikimedia Commons_

\newpage

## GAFAM

\newpage

![](https://framablog.org/wp-content/uploads/2016/06/stickerDiO_gafam.png)
_Source : Framablog_

\newpage

## Câble sous-marin

Ressource :

- [Les câbles sous-marin d’internet](https://veillecarto2-0.fr/2019/01/21/les-cables-sous-marin-dinternet/) (Konan, blog Veille carto, 2019)

\newpage

![Rouleau de câble sous-marins](img/cable-sousmarin-CC-By-Sam-Churchill.jpg)

_Source: [Sam Churchill](https://www.flickr.com/photos/samchurchill/8465534340) 100G-Submarine-Cable - CC-By_

\newpage

![Câbles sous-marins connectés à la France](img/carte-cables-sous-marin-France-by-telegeographics.png)

_Source: Submarine Cable map, [Câbles sous-marins connectés à la France](https://www.submarinecablemap.com/country/france)


## Creative Commons

\newpage

![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Memento_Licences_Creative_Commons.svg/640px-Memento_Licences_Creative_Commons.svg.png)
_Source : Simon Villeneuvefoter, CC BY-SA 3.0 <https://creativecommons.org/licenses/by-sa/3.0>, via Wikimedia Commons_
