---
title:  Le libre pour s'organiser dans les collectifs militants (Faiseuses du web 2021)
author: numahell
date: 28 mai 2021
tags: conférence, numérique, organizing, logiciel libre, numérique libre
separator: <!--s-->
verticalSeparator: <!--v-->
slideOptions:
  transition: slide
  theme: solarized
---

#  Le libre pour s'organiser dans les collectifs militants

<!--v-->

## Présentations

Notes:

Cette présentation a été donnée lors des « Faiseuses du Web » organisée en ligne par Maiwann en mai 2021. Elle a été pré-enregistrée pour faciliter l'organisation.

- [Visionner l'enregistrement](https://aperi.tube/videos/watch/d5ce0f1f-5ac5-47bb-b992-56922c2b06e6)
- [Lancer le diaporama](/numerique-reprendre-controle/slides/faiseuses-web-outils-organiser-collectifs/)

<!--v-->

### Qui suis-je

- développeuse web 
- militante du logiciel libre jusqu'en 2015
- aujourd'hui dans plusieurs collectifs dont Alternatiba, PointCommuns, Le PIC et Framasoft

Notes:

Je suis développeuse web, j'utilise du logiciel libre depuis… 20ans je crois, j'ai milité dans un groupe d'utilisateurs et utilisatrices de logiciels libres pendant presqu'aussi longtemps, notamment pour l'organisation d'un événement annuel entre 2009 et 2015.

En 2016 je m'en suis éloignée pour rejoindre des militances plus larges que le logiciel libre, telles l'écologie et les communs.

Aujourd'hui je fais donc partie notamment :

- de [Alternatiba](https://alternatiba.eu/) (un mouvement citoyen pour le climat et la justice sociale) dans le [groupe local Toulousain](https://alternatiba.eu/toulouse/);
- du [collectif point communs](https://pointcom1.encommuns.org/) (un collectif qui met à disposition des services libres pour les collectifs de commonners);
- du [PIC](https://www.le-pic.org/) (un hébergeur web associatif pour les associations à côté de Toulouse, membre CHATONS);
- et de [Framasoft](https://framasoft.org/fr/) (association éducation populaire au numérique et aux communs culturels).

<!--v-->

### Pourquoi ce thème, et avertissements

- envie d'analyser nos pratiques d'organisation et du numérique
- biais de confirmation : convaincue par le logiciel libre
- biais (je sais pas son nom) : profil tech donc facilité d'usage

Notes:


J'avais envie de vous parler des usages du libre dans les collectifs militants : ça fait un moment que j'essaie de proposer des outils libres au sein de ces collectifs pour nous organiser, en particulier à Alternatiba Toulouse, et d'accompagner mes camarades dans leur choix et leur utilisation.

Actuellement j'essaie de prendre du recul pour pouvoir analyser ce qui est fait au quotidien dans ces collectifs militants du point de vue de l'organisation via des outils numériques, libres ou non. Et donc tout ce que je vais dire n'est qu'à l'état de réflexions.

Il y a certainement de nombreux biais dans ce que je vais dire :
- le biais de confirmation : je reste convaincue par la nécessité d'utiliser des logiciels libres ;
- j'ai un profil technique, et donc j'ai des facilités à utiliser la plupart des outils.

<!--v-->

### En résumé

- Pourquoi le libre dans nos collectifs ?
- Quels outils libres utiliser pour nous organiser ?
- Comment choisir ces outils ?
- Quelques bonnes pratiques et trucs à éviter

<!--s-->

## Pourquoi faire l'effort du libre ?

Alors que les outils proposés par les GAFAM sont tellement confortables ?

<!--v-->

### Un logiciel libre c'est

Un logiciel pour lequel on a la liberté de :

1. utiliser
2. étudier
3. copier
4. modifier et distribuer ces modifications

Le code source doit être ouvert pour étudier ou modifier le logiciel.

<!--v-->

### Plusieurs raisons

- protéger votre vie privée et celle de tous les membres du collectif
- cohérence avec vos valeurs
- contribuer pour améliorer les logiciels utilisés

Notes:

Alors pourquoi choisir plutôt un outil libre alors que les outils proposés par les GAFAM et autres sont tellement confortables ? 

Plusieurs raisons à cela :
- protéger votre vie privée et celle de tous les membres du collectif
- parce que votre collectif est critique vis à vis du modèle des GAFAM qui captent l'attention pour diffuser de la publicité, exploitent les données utilisateurs : vous souhaitez avoir une certaine cohérence avec les valeurs de votre collectif
- parce que vous pouvez potentiellement contribuer plus facilement pour améliorer cet outil

<!--v-->

### Pourquoi ce n'est pas le seul critère ?

Le libre n'est pas une condition suffisante.

<!--v-->

#### Trouver un hébergeur

Logiciel libre en ligne : hébergé sur un serveur web, maintenu par vous, ou bien par un hébergeur.

Critères de choix :

- les services fournis,
- la confidentialité,
- le risque de censure / pays où le serveur se situe

Les [CHATONS](https://chatons.org/), Collectif des Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires.

Notes:

Pour faire simple, un logiciel en ligne doit être hébergé sur un serveur web, maintenu par vous, ou bien par un hébergeur.

S'organiser collectivement implique des services collaboratifs en ligne, donc un hébergeur web pour ces services qui réponde également aux critères de respect des données personnelles.

Vous devrez trouver un hébergeur internet pour vos héberger les outils que vous aurez choisis. Les critères peuvent être : les services fournis, la confidentialité, le risque de censure, donc dans quel pays le serveur est situé.

<!--v-->

#### Et aussi

- accessibilité
- convivialité & design
- installation & configuration
- sobriété et attention respectée

Notes:

D'autres critères sont à prendre en compte : est-ce que les outils envisagés sont accessibles ? Est-ce qu'ils sont conviviaux et intuitifs ? Est-ce que c'est compliqué à installer ou à configurer ? Est-ce qu'ils restent sobre énergétiquement parlant ? Est-ce qu'ils respectent notre attention ?

<!--s-->

## Quels outils libres pour nous organiser ?

<!--v-->

### Quels sont les besoins ?

- s'organiser collectivement : membres, réunions, décisions, répartition des tâches
- communiquer en interne : échanger, diffuser des informations
- collaborer : partager des fichiers ou des agendas, éditer des documents
- communiquer vers l'extérieur (pas dans cette présentation)

<!--v-->

### Quels outils pour quels usages ?

<!--v-->

#### Communiquer en interne

~~Plusieurs~~ trop de choix

- listes de discussion mail (Sympa, mailman)
- forum de discussion (discourse, loomio)
- messagerie instantanée (IRC, rocket chat, matrix, mattermost, signal, …)

Notes:

Trop de choix ! La plupart permettent d'avoir des notifications mail. Discourse et Loomio sont utilisables en mobile. Matrix & Rocket.Chat ont une application mobile, et signal est nativement mobile.

<!--v-->

####  Partager des fichiers et des agendas

- Nextcloud est le plus indiqué

- Applications (plugins) : fichiers, agenda, contacts…
    - Deck : « kanban » tableau de cartes
    - Talk : messagerie instantanée et appel
    - Circles : groupes
    - Collective : pages internes
    - Forms : créer des formulaires

- Outils bureautiques d'édition collaborative disponibles : OnlyOffice ou Collabora Online

Notes:

Vous aurez besoin de stocker les documents de votre collectif, et noter les dates de vos réunions et événements.

Nextcloud est le plus indiqué, mais le design pourrait être vraiment amélioré. Il fonctionne avec des applications (plugins) : fichiers, agenda, contacts, tâches, deck… et il peut être très extensible grâce à ça.

Il est possible de connecter à une instance Nextcloud un outil bureautique d'édition collaborative tel que OnlyOffice ou Collabora Online.

<!--v-->

#### Organiser une réunion

- choisir une date : framadate ou sondage de date sur Loomio
- diffuser l'invitation, puis le CR : sur la liste de mail, ou sur le forum Loomio
- outil de visio si besoin : BBB ou jitsi
- pad de prise de note : etherpad ou hedgedoc

Voir sur entraide.chatons.org les outils disponibles, pas besoin d'inscription

Notes:

Pour choisir une date pour votre réunion : framadate est le plus facile, mais si vous utilisez loomio il existe des sondages de type date.

Diffusez ensuite l'invitation sur votre outil de communication interne.

Pendant la réunion, si c'est à distance vous aurez besoin d'un outil de visio (BBB ou jitsi) et d'un pad de prise de note : etherpad ou hedgedoc. Même en présence, il peut être pratique d'utiliser un pad pour la prise de notes, mais ça dépend de la connexion internet.

Après il faudra diffuser le CR, sur votre outil de communication interne préféré.

C'est là qu'on se rend compte que s'organiser seulement avec une messagerie instantanée va être limité, il faut au moins un lieu où mettre de l'information de façon pérenne.

<!--v-->

#### S'organiser collectivement

Fonctionnalités peu connues de Mobilizon

- gestion de groupe et de ses membres
- discussion internes dans un groupe
- collections de liens et de ressources
- publication de billet d'actualités et d'événements

Notes:

Mobilizon regroupe des fonctionnalité méconnues mais utile pour les collectifs.

- La gestion de groupe et de ses membres
- Dans un groupe, l'accès à un forum de discussions internes
- La possibilité d'ajouter des collections de liens et de ressources
- La publication de billet d'actualités en plus des événements
- Les événements non listés, seulement accessibles par liens

Jusqu'à présent je n'ai pas encore eu l'occasion de le tester, mais ça peut être pertinent pour un petit collectif.

<!--v-->

#### Pour aller plus loin

[Voir aussi Atelier au camp climat Toulouse 2020](https://numahell.frama.io/numerique-reprendre-controle/campclimat-outils-militer/)

<!--v-->

### Dans les collectifs que je côtoie

- les outils récurrents : nextcloud, les pads

- trop d'outils de discussion différents

- même à 4 personnes, nous utilisons plus de 3 outils

- organisation type fédération ou type groupes locaux / team nationale → complexité accrue

Notes:

Voici quelques constats que j'en tire pour le moment :

On retrouve des outils récurrents, tels que nextcloud ou encore les pads.

Il existe trop d'outils de discussion différents, parfois 3 peuvent coexister pour un même collectif.

Même un collectif de 4-5 personnes, nous utilisons plus de 3 outils pour nous organiser : site web, chat, pads, nextcloud

La complexité augmente si on est dans une organisation type fédération ou type équipe nationale + des groupes locaux. En effet la team nationale ou la fédération n'utilisent probablement les mêmes outils de communication que ses groupes locaux ou ses membres, et il faut faire le lien.

<!--s-->

## Comment choisir un ou des outils numériques ?

Comment on choisit un outil numérique ?

Avertissement : j'en suis au début de ma réflexion

<!--v-->

### Comment choisir ?

Proposition méthodologique

- priorisez vos critères de choix en fonction de vos besoins
- sélectionnez les outils candidats en fonction de ces critères
- présentez-les au collectif
- faites-les tester par des volontaires
- choisissez, donc renoncez :)

Comment on choisit un outil numérique ?

Notes:

Voici une proposition méthodologique, que j'ai pu rencontrer lorsque la team Alternatiba nationale a choisi son outil de communication interne, en l'occurrence Rocket.Chat. Même s'il n'est pas complètement adopté, notamment par les groupes locaux, cette façon de procéder a permis une large adoption.

- priorisez vos critères de choix en fonction de vos besoins
- sélectionnez les outils candidats en fonction de ces critères
- présentez-les au collectif
- faites-les tester par des volontaires
- choisissez, donc renoncez :)

<!--v-->

### Anticipez l'adoption de l'outil

Un outil sera plus facilement adopté si :
- il est déjà utilisé par ailleurs
- vous l'avez choisi collectivement
- vous l'avez fait testé par des volontaires

Attention : nos camarades ne sont pas des beta-testeur⋅euse⋅s

Notes:

Attention : tester ne signifie pas le mettre en place immédiatement et yolo, les militant⋅e⋅s ne sont pas des beta-testeur⋅euse⋅s

<!--v-->

### Éviter la surcharge numérique

Dans l'idéal :

- 3 outils maximum si possible
- 1 seul pour la discussion interne

Notes:

Dans l'idéal qui n'existe pas encore, si possible limitez à 3 le nombre d'outils maximum utilisés par l'ensemble du collectif. Ça peut être plus parce que certains groupes de travail auront besoin d'outils spécifiques.

Et surtout un seul pour la discussion interne, du moins un principal.

<!--v-->

### Connaître vos utilisateur⋅ices

- leur facilité ou non à utiliser le numérique
- les outils qu'iels utilisent déjà
- le matériel (smartphone ou PC) qu'iels utilisent le plus souvent
- leurs façons préférées de recevoir l'information (notifications, mails…)
- leur préférence sur l'organisation de l'information reçue : plus ou moins structurée
- leur souhait de ne pas être trop envahi par le numérique

<!--v-->

#### Les difficultés rencontrées

- Les gens n'ont pas envie

- Appréhension, infobésité

- Problème de matériel ou connexion

- Outils inadaptés : pas intuitif, non accessible, pas disponible pour un smartphone

Notes:

« le libre ça marche pas on veut de l'efficace », « non mais nous on fait de la com' donc on n'utilise pas d'outils libres »,  « je connais bien tel outil pourquoi je devrais aller ailleurs », « je ne me suis pas encore connecté⋅e j'ai oublié comment faire »

« l'informatique j'y connais rien à rien », « je m'y perds avec tous ces outils », « je souffre de trop de numérique »

« j'ai pas le matériel / la connexion suffisante »

« je ne peux pas utiliser ce logiciel, il n'est pas accessible pour moi qui suis aveugle », « cet outil est vraiment compliqué à utiliser et pas intuitif », « cet outil est galère à utiliser depuis un smartphone »

« non mais là on a trop d'outils numériques non ? »

<!--s-->

## À l'usage, comment ça se passe

<!--v-->

### Accompagnez

- plusieurs personnes pour administrer
- plusieurs personnes pour animer / modérer
- mini-formations ou questions / réponses

Notes:

Je vous conseille d'être à plusieurs pour administrer les outils en place, au cas où l'une de vous n'est pas disponible, mais également pour animer et modérer l'outil de discussion.

Proposez des mini-formations ou des sessions de questions / réponses régulièrement.

<!--v-->

### Documentez

- page « gare centrale » : point d'entrée pour votre organisation
- documentation pour utiliser les outils (ou liens vers une documentation existante)
- documentation pour configurer et administrer les outils

Notes:

Une page « gare centrale » est bienvenue pour lister les outils utilisez, mais pas seulement, elle peut être utile pour expliquer votre fonctionnement.

Documentez l'utilisation des outils, mais également comment les configurer et les administrer.

<!--v-->

### Requestionnez vos usages

- Un outil n'est pas / plus utilisé
- Votre collectif a évolué
- Les outils ont évolué, ne sont plus adaptés


<!--s-->

## Ressources

- [Guide Resolu](https://soyezresolu.org/)
- [Metacartes.net](https://soyezresolu.org/)
- [documentation framasoft](https://docs.framasoft.org/fr/)
- [CHATONS](https://chatons.org/) : [entraide.chatons.org](https://entraide.chatons.org/fr/) [forum](https://forum.chatons.org/) et [wiki](https://chatons.org/)
- [MOOC Chatons](https://mooc.chatons.org/) : _« Internet : pourquoi et comment reprendre le contrôle ? »_

<!--s-->

## Discussion
