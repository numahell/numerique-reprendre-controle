---
title : Outils en ligne libres pour militer et s'organiser (Camp climat 2021)
summary: Outils numériques en ligne libres pour militer et s'organiser, comment reprendre la main
tags: Logiciel Libre, Alternatives, Organizing, Militer
separator: <!--s-->
verticalSeparator: <!--v-->
slideOptions:
  transition: slide
  theme: solarized
---

# Outils libres pour militer et s'organiser

Présentation et ateliers de prise en main de plusieurs outils numériques et services en lignes libres pour militer, s'organiser et communiquer.

**Comment reprendre la main sur le numérique**

Notes:

Présentation et ateliers de prise en main de plusieurs outils numériques et services en lignes libres pour militer, s'organiser et communiquer.

Nombre de participants : 15 max
Temps : 2h30
Matériel : Ordinateur personnels, videoprojecteur si possible

[Lancer le diaporama](/numerique-reprendre-controle/slides/campclimat-outils-militer/)

Les outils présentés seront soit rapidement abordés, soit plus en détail selon la demande. Pour chaque outil on parlera des hébergeurs éthiques qui proposent ces services.

<!--s-->

## Sommaire

### Présentations (10min)

<!--v-->

### Atelier connaissances sur le numérique (40min)

<!--v-->

### 1. Organiser une réunion (20 min)

https://entraide.chatons.org

- Trouver une date pour vos réunions ou un sondage classique : framadate (10 min)
- Édition collaborative : pads, calc, cartes mentales
    - présentation (5 min)
    - atelier créer un pad (10 min)
    - captures écran carte mentale et calc ? (5 min)
- discussion audio et vidéo
    - jitsi, quelques instances

<!--v-->

### 2. Stocker et partager des informations (20min)

- Partage de fichiers
    - atelier nextcloud (10 min)
    - édition collaborative (5 min)
- Partage de cartes (bonus)

<!--v-->

### 3. Communiquer dans le collectif (20 min)

- liste de mail
    - démo création liste sur Alternatiba
- forum de discussion
- discussion instantannée
    - démo rocket chat Alternatiba / ANV
    - discussion via ordiphone : Signal
- Formulaire en ligne : framaforms (bonus)

<!--v-->

### Bonus : communiquer vers l'extérieur

- media sociaux fédérés
    - démo Mobilizon
 
<!--s-->

## Introduction

<!--v-->

### Où trouver les outils

- [degooglisons-internet.org](https://degooglisons-internet.org/) par Framasoft
- [entraide.chatons.org](https://entraide.chatons.org/) par le collectif des CHATONS
- [Point Com1](https://point-communs.initiative.place/des-outils/) : chat, wiki, forum, video
- …

<!--v-->

### Quelques précautions

- éviter la multiplicité des outils (fatigue informationnelle)
- documenter les outils utilisés et leur mode d'emploi
- faire attention que chacun⋅e ait accès aux outils (diversité dans les niveaux)
- être à l'écoute si des difficultés surviennent

<!--s-->

## Organiser une réunion

<!--v-->

### Trouver une date pour les réunions / sondage simple : framadate

Démo : créons un sondage pour trouver la date d'une rencontre / discussion sur le numérique et le militantisme

https://framadate.org/

<!--v-->


![Framadate](./img/framadate.png)

<!--v-->

### Rédaction collaborative avec Etherpad

![Capture Framapad](./img/framapad.png)

<!--v-->

#### En pratique

1. Aller sur l'adresse  
      [https://lien.bechamail.fr/campclimat](https://lien.bechamail.fr/campclimat)
3. Écrivez !

Pour créer votre propre pad :

1. créer un pad sur [framapad.org](https://framapad.org/) (choisissez quotidien parce que vous allez juste tester)
2. envoyer le lien à vos contacts
3. écrire !

Service disponible sur [entraide.chatons.org](https://entraide.chatons.org)

Notes:

À savoir : [Mypads](https://mypads.framapad.org/) permet de partager des pads de façon restreinte

Voir aussi la documentation [chez les CHATONS](https://wiki.chatons.org/doku.php/la_redaction_collaborative_avec_etherpad)

À savoir : il existe d'autres systèmes de pad avec des fonctionnalités différentes, par exemple CodiMD (voir sur [pad.lescommuns.org](https://pad.lescommuns.org/)) ou [Cryptpad](https://cryptpad.fr/)

<!--v-->

### Discussion en audio et visio-conférence :<br>Mumble, Jitsi, Big Blue Button

<!--v-->

#### Mumble : [serveur du PIC](https://audio.le-pic.org/)

- Conversation audio uniquement, plus économe
- accès via un logiciel bureau ou un site web si disponible
- ergonomie à améliorer, mais fait le job
- retrouver les instances sur https://entraide.chatons.org/

<!--v-->

Instance Mumble au PIC https://audio.le-pic.org/

![Mumble web au PIC](./img/mumble-web-pic.png)

<!--v-->

#### Jitsi

Conversation vidéo, mais possible de la désactiver

![Jitsi](./img/jitsi.png)

<!--v-->

- retrouver les instances sur https://entraide.chatons.org/
- disponible également chez Liiibre : https://meet.liiib.re/

<!--v-->

#### Big Blue Button

- très utile pour les cours ou conférences en ligne

![Big Blue Button](./img/bigbluebutton.png)

<!--v-->

Une liste d'instances

- https://bbb.faimaison.net/b
- https://ensemble-bbb.scaleway.com/
- https://greenlight.lal.cloud.math.cnrs.fr/b (pour les personnels de l’Enseignement supérieur et la Recherche)
- https://bbb.fdn.fr/b

*Source : https://wiki.chatons.org/doku.php/la_visio-conference_avec_big_blue_button*

<!--s-->

## Stocker et partager des informations

Nextcloud : partage de fichiers, agenda, contacts, mais d'autres fonctionnalités disponibles

<!--v-->

### Partager de fichiers : Nextcloud

Selon les hébergeurs, l'édition collaborative est disponible

<!--v-->

#### Démo

Utilisons l'instance du Pic

1. créer un dossier
1. le partager via un lien public
1. autoriser l'édition, et créer un tableur

<!--v-->

#### Atelier

1. Utilisateurs disponibles : toto5 à 9, renseigner les emails
1. envoyer des fichiers
1. éditer un tableur ou un document (si édition collaborative)

<!--v-->

### Partager un agenda : Nextcloud

- Créer un agenda
- Le partager à d'autres utilisateurs ou groupes

<!--v-->

### Les app de Nextcloud

- Calendriers
- Gestion des contacts
- Gérer des tâches
- app desk

Mais aussi

- Cospend
- Talk (messagerie instantannée)
- Collectives (travail de groupe)
…

<!--v-->

### Instances Nextcloud disponibles

Chez les [CHATONS](https://www.chatons.org/)

- Le PIC
- Devloprog
- Zaclys
- Marsnet
- Indie Hosters
- …

<!--v-->

### Créer et partager des cartes :<br>Gogocarto, Umap

<!--v-->

#### [Umap](https://umap.openstreetmap.fr/fr/)

- très bien pour faire une carte rapidement
- exemple carte potagers partout

![](./img/umap-PotagersPartout.png)

<!--v-->

#### [Gogocarto](https://gogocarto.fr/projects)

- édition des données avancée et affichage élaboré
- nombreuses cartes, par exemple celle des [groupes locaux Alternatiba et ANV](https://collectifs-locaux-alternatiba-anv.gogocarto.fr)

![](./img/gogocarto-collectifsLocauxAlternatibaANV-COP21.png)

<!--v-->

Vous connaissez [transiscope](https://transiscope.org/) ? la carte des alternatives, regroupant les données de plusieurs collectifs et organisations, utilise Gogocarto.

![](./img/transiscope.png)

<!--s-->

## Communiquer dans le collectif

<!--v-->

### Discussions asynchrones :<br>listes de mails et forums

<!--v-->

#### Listes de discussion: Sympa

- pratique pour n'oublier aucun⋅e destinataire
- plutôt à réserver aux mails important
- attention à la surchage de mails, évitez les pièces jointes volumineuses (> 5Mo)

<!--v-->

#### Démo Sympa

Utilisons l'instance de Alternatiba Toulouse, hébergée par le Pic

1. Créer une liste dédiée à l'atelier
1. Ajouter les adresses emails des participant⋅e⋅s
1. Envoyer un message

<!--v-->

#### Forum de discussion

Deux outils pas mal utilisés, fonctionnalités de groupes restreints, d'édition de texte riche…

- Discourse (forum.lescommuns.org, framacolibri)
    - édition possible en mode wiki
- Loomio (framavox.org)
    - sondages et votes avancés

<!--v-->

##### Démo discourse : [forum.lescommuns.org](https://forum.lescommuns.org)

Catégorie d'un forum : liste des sujet de discussion

![Forum catégorie](./img/forum-lescommuns-1.png)

<!--v-->

Un sujet de discussion

![Forum discussion](./img/forum-lescommuns-2.png)

<!--v-->

##### Démo Loomio : [framavox](https://framavox.org/alternatiba)

Accueil du groupe mini-village 4 Toulouse

![Loomio groupe](./img/framavox-1.png)

<!--v-->

Un sujet et ses réponses

![Loomio groupe](./img/framavox-2.png)

<!--v-->

### Discussion instantannée :<br>RocketChat, Signal

<!--v-->

#### RocketChat (et ses cousins Matrix et Mattermost)

- très utile pour les discussions quotidiennes, car moins formel que le mail
- édition de texte riche, prévisualisation des liens, partage de fichiers et de gifs
- application mobile disponible (pour Matrix et Mattermost également)
- inconvénient : difficile de rechercher une information passée

<!--v-->

#### Signal

- équivalent de Telegram

<!--v-->

#### Démo RocketChat : [chat.alternatiba.eu](https://chat.alternatiba.eu)

![RocketChat](./img/rocketChat.png)

<!--v-->


<!--v-->

### Communiquer vers l'extérieur :<br>media sociaux fédérés

- Mastodon (microblogging)
- Peertube (vidéo)
- Mobilizon (événements)
- …

<!--v-->

#### Le principe

- Des outils pour plusieurs usages (blog, microblogging, vidéo) …
- … Administrés par différentes personnes, sur différents serveurs, avec différentes règles d'utilisation …
- … Qui peuvent intéragir les uns avec les autres (commentaires ou publication croisés)

<!--v-->

![Fediverse](./img/fediverse.jpg)

<!--v-->

#### Mastodon (microblogging)

Voir le compte Alternatiba Toulouse : https://mastodon.tetaneutral.net/@AlternatibaT

![Mastodon, réseau social qui vous redonne le contrôle](./img/mastodon-alternatiba-toulouse.png)

<!--v-->

#### Peertube (vidéo)

Par exemple, les [Conférences gesticulées](https://tube.conferences-gesticulees.net/)

![](./img/PeerTube.png)

<!--v-->

#### Mobilizon (diffusion d'événements) : à venir fin 2020 !

- Pour éviter d'inviter des gens sur Facebook parce qu'on y publie nos événements…
- Beta en place, gestion des événement et multi-identité
- V1 sortie fin 2020, gestion des groupes, fonctionnalités de discussions

<!--v-->

![Mobilizon](./img/mobilizon.png)

<!--v-->

Démo Mobilizon sur [rdv.rangueil.net](https://rdv.rangueil.net/)

1. Créer un compte
1. Créer un événement

<!--s-->

## Compta et gestion d'adhésions

<!--v-->

### Gestion des adhérents : [Galette](https://galette.eu)

Édition d'un membre

![Galette édition d'un membre](./img/galette-edit_member.png)

<!--v-->

### Gestion avancée

- [Garradin](https://garradin.eu/), orienté association
- [Dolibarr](https://www.dolibarr.org), association, TPE/PME, etc.

<!--v-->

#### Garradin

Fiche adhérent

![Garradin fiche adhérent](./img/garradin-membre.png)

<!--v-->

Comptabilité

![Garradin compta](./img/garradin-compta.png)

<!--v-->

#### Dolibarr

Liste des adhérents

![Dolibarr liste adhérents](./img/dolibarr-adherent.png)

<!--v-->

Écritures bancaires

![Dolibarr compta](./img/dolibarr-compte-ecritures.png)

<!--s-->

## Des hébergeurs éthiques

<!--v-->

### Hébergeurs de services en ligne

Les [CHATONS](https://chatons.org/) (Collectif des Hébergeurs Alternatifs Transparents, Ouverts, Neutres et Solidaires)

![Les CHATONS](./img/chatons.png)

<!--v-->

Certains de leurs services directement accessibles via [entraide.chatons.org](https://entraide.chatons.org/)

![Entraide CHATONS](./img/entraide-chatons.png)

<!--v-->

Quelques services proposés pour les Communs par le [Collectif PointComm1](https://point-communs.initiative.place/)

- chat
- forum
- wiki
- vidéo

<!--v-->

### Hébergeurs pour louer un serveur

Plus complexe, plusieurs critères

- énergies renouvelables
- lois sur les données selon les États (loi Suisse, Allemande ou Pays-Bas les moins contraignants)

Gandi, Infomaniak, Hertzner…

<!--s-->

## Ressources

- [MOOC des CHATONS](https://mooc.chatons.org/)
- [Annuaire du libre](https://framalibre.org/)
- [Les alternatives à Framasoft](https://alt.framasoft.org/fr/)
- [entraide.chatons.org](https://entraide.chatons.org/fr/)
- [Guide RESOLU](https://soyezresolu.org) par Framasoft, les CEMEAs
