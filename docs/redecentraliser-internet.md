---
title: Redécentraliser Internet (Podcast MOOC Écrire sur le Web)
author: numahell
date: 28 mai 2021
tags: podcast, numérique, cours en ligne, logiciel libre, Internet, CHATONS, GAFAM, Décentralisation, MOOC, Framasoft
id: 20210616213022
---

# Redécentraliser Internet

Podcast radio UPLOAD WE01, Écrire sur le web du 2 juin 2021. Ce texte est la trame écrite que j'ai rédigée pour cette intervention radio.

- Retrouver [le podcast](https://aperi.tube/videos/watch/6bd46fbf-86bd-43a7-8808-0dcfeef76bf3)
- Visiter le [MOOC écrire, communiquer et collaborer sur le Web](https://librecours.net/parcours/we01-001/)

## Internet, y a rien de plus fragile… mais c'est quoi Internet ?

Lorsque Google "éternue", lorsque ses services ne sont plus accessibles pendant quelques heures, ce sont des centaines de milliers de personnes qui ne peuvent plus accéder à leurs mails.

En mars dernier, un des 33 datacenters de l'hébergeur français OVH a pris feu. Les données de milliers de sites sont partis en fumée en une nuit. Si la plupart ont pu remettre en place leur sites sur d'autres serveursn grâce à leurs sauvegardes, un certain nombre ont littéralement tout perdu.

Facebook concentre les données personnelles d'1.5milliards de personnes, dont 40millions de français. Lorsqu'une liste de millions de numéros de téléphone fuite… ça fait tâche.

Lorsqu'un service de renseignement (la NSA) met un mouchard sur un tuyau d'Internet (partant du Danemark), c'est le traffic de tout le continent qu'il peut espionner.

Quel est le point commun de tous ces événements ? c'est un impact démesuré sur des millions de personnes en même temps, alors qu'une seule entreprise ou État est concerné.

Comme me le disait ma tante il y a quelques années :

> « Internet ?! y a rien de plus fragile ! »

**Ok, mais c'est quoi Internet ?**

### Client, serveur

Pour rappel, un **serveur** est un ordinateur connecté en permanence sur Internet, et contient des logiciels qui rendent un **service** particulier. On a des serveurs de plusieurs sortes : 

- serveur mail, pour vous permettre d'envoyer et de recevoir des mails,
- serveur de fichiers, vous avez peut-être entendu parler du "ftp", ou "File Transfert Protocol", en français le "protocole de transfert de fichiers"
- serveur web qui permet de vous envoyer le contenu des pages, ensuite affichées par votre navigateur

Vous, ou plutôt votre ordinateur, a le rôle de **client** lorsqu'il demande un service à ce serveur. Votre navigateur par exemple envoie une **requête** vers un serveur lorsque vous souhaitez afficher une page web. Ce serveur, s'il est disponible, lui enverra une **réponse** contenant la page web que vous recherchez. Et un seul serveur va pouvoir traiter les requêtes de très nombreux clients à la fois.

### Le Datacenter

Des serveurs rangés dans des armoires appelées des **baies**, des armoires stockées dans un seul lieu, un grand immeuble par exemple : c'est le **datacenter**.

### Internet, le réseau des réseaux

**Internet** permet d'interconnecter des réseaux dans le monde entier. Notre maison elle-même est un réseau : la télé, un ou plusieurs téléphone mobile ou ordinateur connectés entre eux par une box, qui elle est connectée au réseau de notre opérateur, lui-même connecté à d'autres réseaux, d'autres opérateurs, des datacenters, etc.

## Internet des GAFAM, minitel 2.0 : comment en est-on arrivés là ? 

### Internet, des promesses de libertés

Lorsque Internet est devenu accessible au grand public dans les années 95-2000, les internautes s'exprimaient sur des newsgroups, sur des forums de discussion, et écrire des articles de blogs. Recevoir de l'information passait par des newsletters ou des flux RSS. À cette époque, Google n'était qu'un moteur de recherche parmi d'autres, Facebook et Twitter n'existaient pas, et la connexion à Internet ne nous permettaient pas de visionner de la vidéo si facilement.

Il faut pourtant garder à l'esprit que ce n'est qu'une petite partie des internautes qui s'expriment et échangent via Internet par rapport à une majorité uniquement consommatrice, et que les hippies espérant une émancipation par la technologie sont finalement devenus pour certains des chefs d'entreprises capitalistes et prédatrices, prônant l'individualisme et un certain solutionnisme technologique.

### Plateformisation de l'information

À partir de 2004, des plateformes ont émergé sur la toile, et commencé à concentrer l'activité des internautes :  Facebook (lancé en 2004), puis la vidéo avec Youtube (racheté par Google en 2006), le microblogging avec Twitter en 2007. Tant que ces plateformes étaient là parmi d'autres, ce n'était pas encore un problème.
Mais pour gagner de l'argent, ces plateformes ont cherché à capter notre attention, puis à hierarchiser l'information à l'aide d'algorithmes pour mettre à notre vue les contenus les plus engageants, à nous garder en ligne en récompensant nos cerveaux grâce aux likes des autres utilisateurs, dans le but de nous faire cliquer sur les publicités.

L'apparition des smartphones (en suivant le premier IPhone) a prolongé notre temps de connexion et d'interactions sur ces plateformes.

Aujourd'hui nous confondons souvent le web avec Google ou Facebook, les vidéastes se nomment des youtubeurs… Ces plateformes constituent un monopole et exercent sur notre société une quadruple domination :

- domination technique
- domination économique : les GAFAM peuvent racheter n'importe quelle entreprise qui porte un service qui pourrait lui faire de l'ombre
- domination culturelle
- domination politique

<!-- FIXME préciser chaque types de dominations -->

### Surveillance d'États

On peut dire que les données que nous offrons aux GAFAM, c'était littéralement le rêve de la Stasi, organe de renseignement de l'Allemagne de l'Est.

Aujourd'hui les États sont tentés de surveiller massivement leurs ressortissants, et le font pour certains. Prism ou Amesys, sont des dispositifs techniques permettant d'inspecter les tuyaux d'Internet : la concentration des réseaux et des acteurs est une aubaine pour eux.
Amesys est un logiciel français permettant de surveiller les communications qui transitent sur les réseaux Internet, s'exporte dans plusieurs pays, [notamment en Lybie](https://reflets.info/articles/le-contrat-amesys-en-libye-grave-dans-le-marbre) et [en Syrie](https://www.nextinpact.com/article/46118/syrie-pourquoi-qosmos-a-beneficie-dun-non-lieu).
Le Patriot Act permet aux USA de demander à une entreprise américaine des données sur ses utilisateurs, où que soient ses serveurs.

Les censures administratives sont courantes, parfois même en France : la tentation est grande de se passer d'un juge pour faire fermer des sites web problématiques, et même si les causes peuvent être légitimes au départ (contre des sites web pédophiles ou négationistes), elles sont malheureusement étendus à d'autres catégories de sites (activistes écologiques, etc). Il est dangereux de modifier la législation pour permettre cela, malheureusement, c'est ce qui a été fait en France depuis une quinzaine d'années.


## Comment se Dégafamiser : un pas après l'autre

Bien sûr s'émanciper des GAFAM ne peut se faire d'un seul coup, c'est comme si vous vous mettiez à faire 30km de vélo par jour dès demain, ou deveniez vegan en un jour. Mais il est possible de le faire petit à petit, en se fixant des objectifs atteignables.

Pour commencer, quel logiciel utilisez-vous pour naviguer sur Internet ? Google Chrome, Edge de Microsoft, ou bien Firefox de Mozilla ? si vous utilisez les deux premiers, installer Firefox à la place est déjà un bon premier pas : l'interface est très similaire.

Et votre moteur de recherche, quel est-il ? La plupart des navigateurs proposent Google par défaut, mais vous pouvez modifier cela dans les paramètres de votre navigateur : Menu > préférences > section Moteur de recherche. Choisissez par exemple [Duck Duck go](https://duckduckgo.com/) ou [Startpage](https://startpage.com).

### Auto-défense numérique

L'étape suivante peut être de se soustraire des publicités et des sites espions (appelés également "trackers"). Ces sites espions sont sollicités par un composant sur une page web, qui va permettre obtenir certaines informations sur votre configuration et votre navigation.

Vous pouvez vous prémunir de ça en activant la protection renforcée dans vos préférences, et en installant des extensions sur votre navigateur comme [UBlock Origin](), [Privacy Badger]().
Vous pouvez isoler du reste de votre navigation les sites de plateformes problématiques grâce aux conteneurs isolés Facebook container ou Google Container.
Le site [Security in a box](https://securityinabox.org/fr/guide/firefox/linux/) vous propose un guide en français.

Les conditions d'utilisation sont trop longues à lire ? Un site communautaire en fait des résumés : [tosdr.org](https://tosdr.org/). Sur mobile également, l'application [Exodus privacy](https://exodus-privacy.eu.org/fr/) audit les applications installées sur votre smartphone en listant pour chacune les pisteurs et les autorisations.

Si vous suivez quelques bonnes pratiques d'auto-défense numérique, notamment celles proposées par [Nothing2Hide](https://wiki.nothing2hide.org/doku.php?id=protectionnumerique:start), ce sera un très bon début. Le [Guide Boum](https://guide.boum.org/) est une ressource très complète pour vous y aider.

### Des alternatives

Il existe des alternatives à certains services proposés par les GAFAM. L'édition collaborative avec des pads a des fonctionnalités qui permettent de se passer de Google docs, ou bien la vidéo via Jitsi permet d'éviter Zoom pour des petites visio conférences.

Ces services libres sont basés sur des [logiciels libres](https://mooc.chatons.org/mod/lesson/view.php?id=50&pageid=15), c'est à dire des logiciels dont la licence vous permet de les utiliser, étudier, copier, modifier et diffuser ces modifications sous la même licence.
Une liste de services libres pour collaborer, et ne nécessitant pas de s'inscrire est proposée sur [entraide.chatons.org](https://entraide.chatons.org/fr/) par le collectif des CHATONS.

Il faut bien comprendre que les logiciels libres ne sont pas identique à leur homologues propriétaires, qu'ils ne proposeront pas exactement les mêmes fonctionnalités, et que parfois l'utilisabilité ne sera pas aussi fluide.
Rester chez les GAFAM procure un certain confort, changer nos habitudes pour aller vers le libre afin de nous émanciper nous demande un effort.

> Comparer un logiciel libre à un logiciel propriétaire, c'est comparer une solution qu'on consomme à une solution qu'on construit - Pouhiou

Il faut également qu'ils soient chez un hébergeur transparent et respectueux ce vos données. J'ai parlé du collectif des CHATONS : initié par Framasoft en 2016 suite à sa campagne degooglisons, ce collectif est composé d'au moins 80 membres qui suivent une charte précise, avec des critères et leur mise en œuvre opérationnelle. Ces critères sont dans l'acronymes : Collectifs des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires.

Certains sont restreints aux membres de leur famille, tandis que d'autres sont ouverts à tous. Pour la plupart, pour quelques euros par mois vous pourrez accéder à des services tels que Nextcloud, un outil de partage de fichiers, de calendrier, d'édition collaborative (avec OnlyOffice), etc. Parmi les CHATONS, nous pouvons citer Picasoft bien sûr, mais aussi Zaclys, Marsnet, ou encore le PIC dont je fais partie.

L'étape suivante ? Acheter votre propre nom de domaine, certains revendeurs (on dit registrars) proposent avec une ou plusieurs boites mails, tout cela pour une quinzaine d'euros par an. Quelques CHATONS également proposent un service mail, même s'ils ne sont pas très nombreux.

Oui l'hébergement a un coût, si les services des GAFAM sont gratuits, il faut se demander qui est le produit, et qui sont ses vrais clients.

## S'héberger soi-même : ça se tente !

Il est possible de rejoindre un CHATONS… pour y contribuer ! C'est un très bon moyen d'apprendre comment fonctionne l'administration d'un serveur, et même si vous n'osez pas parce que vous n'y connaissez rien, le regard extérieur et les compétences autres que techniques leur sera forcément utiles.

Vous pouvez également louer un serveur chez un hébergeur de votre choix, en veillant à ce que cet hébergeur n'ait pas pour pratique de revendre vos données. Vous pouvez choisir cet hébergeur également sur des critères écologiques, s'il réemploie le matériel sur la durée, ou utilise des énergies renouvelables.

Un serveur (ou une baie de serveurs) peut très bien être posé dans votre salon, dans une cave ou un local associatif. L'essentiel est que le local doit être aéré, accessible à vous-même et uniquement à des personnes de confiance, tout en étant protégé des intrusions. Attention si vous hébergez plusieurs serveurs dans la même pièce, ils produisent de la chaleur, et peuvent dysfonctionner à cause de cela.
Question puissance, après tout votre ordinateur a la même puissance qu'un bon nombre de serveurs, donc s'il avait jusqu'ici uniquement le rôle de client, il peut très bien devenir à son tour un serveur.

Que votre serveur soit dans votre salon ou dans le datacenter que votre hébergeur, vous pouvez utiliser Yunohost pour faciliter le travail d'administration système. Ce logiciel permet d'installer facilement des services web et de les relier à votre nom de domaine. C'est adapté pour vous aider à administrer votre serveur en toute autonomie si vous n'êtes pas à l'aise avec l'administration système.


## Solutions politiques, solutions collectives

### Le droit et la régulation

En mai 2018 a été mis en place au niveau européen le RGPD, le Réglement Général de Protection des Données. Entre autres choses, ce réglement permet de définir ce qu'est une donnée personnelle (nom, prénom, adresse IP…) et une donnée sensible (religion, orientation sexuelle…). Il oblige toute organisation d'indiquer quelles données personnelles elle collecte, dans quel but et pour combien de temps, et de demander à l'internaute son consentement éclairé. Les sanctions sont sensées être dissuasives pour responsabiliser les entreprises.

Si cela a permis de définir des règles communes au niveau européen, et de préciser ce qu'est une donnée personnelle et une donnée sensible, les plateformes comme Facebook ou Twitter ou Instagram ne nous permettent pas de créer un compte sans leur céder nos données. Les recours et actions de groupes engagés par de nombreuses ONG dont la Quadrature du Net n'ont pour la plupart malheureusement pas abouti, et certains estiment que c'est la limitation de la collecte des données qu'il faudrait instaurer dans le droit.

En cas de contenus illicites, les sanctions touchent indifféremment un petit hébergeur CHATONS et Twitter ou Facebook, aussi la Quadrature du Net propose de distinguer dans le droit les hébergeurs simples des plateformes qui hiérarchisent l'information à notre place via les alogorithmes, et se comportent ainsi comme des éditeurs de contenu.

Lutter contre les monopoles des géants d'Internet n'est pas chose aisée : les lois anti-trust américaines n'ont pas permis d'éviter la concentration des GAFAMS, il est donc question de réformer ces lois à l'avenir. D'autres propositions sont évoquées : exiger plus de transparence et des conditions d'utilisation claires, réguler en fonction de chaque modèle économique (commerce pour Amazon, publicité pour Facebook et Google), contrôler les fusions ou rachats opérés par les GAFAM, voire les démanteler en plus petites organisations.

Des tentatives d'appliquer des règles fiscales plus contraignantes aux géants du web sont également évoquées. En avril dernier Biden propose d'adopter un minimum d'imposition au niveau mondial, mais également de faire payer des impôts suplémentaires sur les revenus réalisés dans les paradis fiscaux, ou dans des pays pratiquant le dumping fiscal. Autre piste : taxer au-delà des seuls bénéfices, en les taxant sur le nombre de clients dans chaque pays.

Cela fait beaucoup d'intentions, de déclarations, qui prennent beaucoup de temps et de négociations entre États pour être concrétisées, ce ne sera certainement pas suffisant mais ce travail est pourtant nécessaire : si on ne s'occupe pas des lois, elles s'occuperont de nous.

- [Faut-il régulier Internet 1/2](https://www.laquadrature.net/2020/11/01/faut-il-reguler-internet-1/) et [2/2]()
- [Interopérabilité](https://framablog.org/2019/06/12/cest-quoi-linteroperabilite-et-pourquoi-est-ce-beau-et-bien/)
- [Démanteler les GAFA ?](https://www.lemonde.fr/blog/internetactu/2018/02/10/demanteler-les-gafa/) sur Internet Actu
- Joelle Toledano [interventions sur FCulture](https://www.franceculture.fr/personne/joelle-toledano) et [Reprendre le pouvoir aux GAFA (la bibliothèque idéale)](https://www.franceculture.fr/emissions/la-bibliotheque-ideale-de-leco/reprendre-le-pouvoir-aux-gafa-avec-joelle-toledano)

### Les actions collectives

Les actions collectives sont là aussi, et sont nécessaires pour mieux résister et s'émanciper des GAFAM.

Les fournisseurs d'accès Internet associatifs se sont organisés en fédération, avec [la FFDN](https://www.ffdn.org/). Cela leur permet d'avoir une certaine autonomie et un ancrage local pour raccorder leurs membres à Internet et fabriquer des bouts de réseau, tout en mutualisant les connaissances techniques ou juridiques, ainsi qu'un certain nombre de moyens transverses.

On en a parlé un peu avant, près de 80 organisations se sont regroupées dans le Collectif des Hébergeurs Alteratifs, Transparents, Ouverts, Neutres et Solidaires ([CHATONS](https://chatons.org/)), répondant à une charte précise sur les garanties et la transparences apportées à leurs adhérents ou utilisateurs et utilisatrices. Cela leur permet également de s'entraider sur des questions techniques, mais également organisationnelles.

Le **fediverse[^fedi]** (mot-valise pour federated universe) est un ensemble de réseaux sociaux alternatifs qui communiquent les uns avec les autres. Ils fonctionnent grâce à des logiciels libres, et chaque nœud ou instance peut choisir d'être sans publicité, de préserver nos vies privées et ne filtrent pas le contenu auquel on a accès. C'est un peu le principe de fédération appliqué au numérique.

[^fedi]: https://mooc.chatons.org/mod/lesson/view.php?id=49 

Antonio Casili ainsi que Trebor Scholz invitent à créer des plateformes publiques, ou collectiviser des plateformes existances pour lutter contre l'uberisation. C'est ce qu'ils appellent le **[cooperativisme de plateforme]( https://mooc.chatons.org/mod/lesson/view.php?id=68)[^coop]**.
Imaginons une sorte de AirBNB fédéré avec les valeurs du couchsurfing, chaque instance étant gérée au niveau de la ville elle respecterait les règlementations et un quota max de logement touristiques.

[^coop]: Coopérativisme de plateforme (ex [plateformes en communs](https://coopdescommuns.org/fr/plateformes-en-communs/) 

Bien sûr tout n'est pas rose dans le monde merveilleux des associations et collectifs, qui reposent majoritairement sur des énergies bénévoles. Bien sûr ces structures représentent peut-être une portion infinitésimale des bénéfices engrangés par les GAFAM. Mais pour moi elles offrent une fenêtre vers un numérique plus juste, alors qu'il est devenu incontournable et imposé à nous.

Le Groupe Marcuse va encore plus loin dans la résistance : ce groupe de réflexion a écrit une critique du numérique et de l'informatisation de la société, qui les amène à proposer de s'extraire le plus possible du numérique, pour aller vers une déconnexion totale.

## Ressources

### Cours en ligne

- [Pouvoirs et numériques, du logiciel libre à la redécentralisation de Web](https://school.picasoft.net/fds/confint/co/conf.html), cours Picasoft, 2018
- [MOOC Chatons](https://mooc.chatons.org)

### Podcasts

#### Libre à vous

Des podcasts sur le libre

- https://april.org/79-Health-Data-hub
- https://april.org/103-Technopolice
- https://april.org/76-sante-et-logiciel-libre
- https://april.org/68-Internet-et-le-droit
- https://april.org/antenne-libre-services-libres-et-ethiques-les-chatons-le-confinement-diffusee-jeudi-19-mars-2020-sur
