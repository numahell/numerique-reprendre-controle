# Contribuer

Le texte source de ce site est situé [dans ce dépôt](https://framagit.org/numahell/numerique-reprendre-controle).

## Signaler une erreur

Vous pouvez le faire en créant un ticket [ici](https://framagit.org/numahell/numerique-reprendre-controle/-/issues), ou en me contactant par mail à reprendre_controle – at – numajules – point – net.

## Soumettre vos modifications

Si vous utilisez git, vous pouvez me transmettre vos modifications via le dépôt.

Ce site est basé sur [mkdocs.org](https://www.mkdocs.org).

### Commandes

* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

### Arborescence

    mkdocs.yml    # Fichier de configuration
    docs/
        index.md  # Page d'accueil
        img/      # Images et figures
