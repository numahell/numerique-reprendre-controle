# Numérique, reprendre le contrôle

Ce site regroupe des interventions que j'ai pu faire dans la thématique du numérique libre et éthique, mais aussi de la critique du numérique. 

- Une émission de radio dans le cadre du MOOC « Écrire sur le web » le 2 juin 2021 :
      [Redécentraliser Internet](./redecentraliser-internet.md)

- Une présentation lors de la conférence « Faiseuses du web » le 27 mai 2021 :
    [Le libre pour s'organiser dans les collectifs militants](./faiseuses-web-outils-organiser-collectifs.md)

- Deux ateliers donnés au Camp Climat Toulouse le 22 août 2020 :
    - [présentation et discussion sur les enjeux](./campclimat-enjeux-numerique.md) : pourquoi reprendre le contrôle
    - [atelier sur des outils en ligne pour s'organiser et militer](./campclimat-outils-militer.md) : comment reprendre le contrôle

## Réutiliser

Vous pouvez reprendre des contenus ou des supports entiers pour vos propres ateliers, c'est la raison même de ce site, selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a>

→ Vous avez trouvé des erreurs ? Comment [contribuer](./contribuer.md)
