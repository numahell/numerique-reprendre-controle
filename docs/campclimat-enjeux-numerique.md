---
title : Les enjeux du numérique et d'Internet (Camp climat 2020)
---


Présentation des enjeux du numérique et des impacts sociaux et environnementaux. Comprendre le rôle des GAFAM, leur intrication avec les États, et le capitalisme de surveillance. Être conscient de l'impact du numérique sur l'environnement. Quelques pistes pour tenter de reprendre la main.

Ce texte est plus une trame pour pouvoir parler du sujet, et contient beaucoup de reprise du [MOOC Chatons](https://mooc.chatons.org/course/view.php?id=3) disponible en CC-By-SA.

**Pourquoi reprendre la main sur le numérique**

## L'informatisation de la société

L'informatique : Traitement automatique de l'information à l'aide de programmes exécutés par des machines[^1].

[^1]: https://fr.wikipedia.org/wiki/Informatique

Le numérique n'a rien de virtuel : 34 milliards d’équipements pour 4,1 milliards d’utilisateurs, soit 8 équipements par utilisateur :

- les smartphones (3,5 milliards), les autres téléphones (3,8 milliards), les dispositifs d’affichages tels que les télévisions, écrans d’ordinateur, et vidéo-projecteurs (3,1 milliards)
- 1,1 milliard de box DSL / fibre, 10 millions d’antennes relais (2G à 5G) et environ 200 millions d’autres équipements
- 67 millions de serveurs hébergés

Source : Rapport de l'étude - Empreinte environnementale du numérique mondial, GreenIT [GreenIT], 2019

Historique rapide ? de la machine à calcul de Ada Lovelace, la machine à décrypter de Alan Turing au système d'exploitation (programmation et systèmes), du transistor au circuit imprimé (composants), du calculateur dans une armoire à la montre connectée (matériels), de ? au très haut débit (réseaux).

Source : Boullier, Dominique. Sociologie du numérique. 2e édition. Collection U Sociologie. Malakoff: Armand Colin, 2019. https://www.armand-colin.com/sociologie-du-numerique-2e-ed-9782200624750.

## Le fonctionnement d'Internet

serveurs, réseaux, hébergeurs, services, cloud

![](./img/Internet_access_diagram.svg)

## Les impacts négatifs

20210803233120

### La domination des GAFAM / NATU / BATX


- Google, Apple, Facebook, Amazon, Microsoft
- 2ème génération : Netflix, AirBnB, Tesla, Uber
- En Chine : Baidu, Alibaba, Tencend, Xiaomi

Leur objectif est le même : offrir des services au plus grand monde, et développer leur empire pour faire grossir leur capital.

Le premier arrivant sur un créneau remporte le jeu, pour cela les géants du web procèdent à de nombreuses acquisitions pour obtenir des technologies, des brevets, des talents ou des réseaux et utilisateurs.

#### Domination technique

Facebook : 1.3 milliard de personnes s'y connectent chaque jour, y laisse plus de 700 millions de commentaires et 200 millions de photos.
Youtube : en 2018, 20% du traffic mondial, 1 milliard d'heure de vidéos diffusées chaque jour.

Le *cloud* (infrastructure permettant d'avoir les fichiers et logiciels accessible en permanence depuis un ordinateur connecté à Internet), largement dominé par Google, Amazon (AWS) et Microsoft (Azure).

![Infographie de @LoriLewis et de @OfficiallyChadd](./img/internet-minute-comparison.jpg)

#### Domination économique

Des modèles économiques différents mais beaucoup de points communs

- La dérégulation des marchés leur ont permis d'atteindre les plus grosses capitalisations boursières mondiales (peuvent même racheter leurs propres actions)
- Trésoreries très importantes : Apple Google et Microsoft détiennent 1/4 du cash des entreprises américaines, Apple dispose de 100Mds de trésorerie (= PIB du Maroc)
- Investissements énormes dans la recherche (comparer le budget du CNRS et de Amazon ou Google)
- S'appuient sur l'économie mondialisée pour concentrer les activités générant le plus de profit (conception, marketing, design), et sous-traiter les activités les moins rentables (fabrication, modération des plateformes de contenus, livraison)

Conséquences :

- elles peuvent racheter n'importe quelle entreprise, donc aucune chance de les concurrencer
- elles se diversifient et rachètent des entreprises de secteurs qu'elles ne touchent pas encore (Microsoft et Facebook s'intéressent beaucoup au secteur de la santé)
- elles font de l'optimisation fiscale sans crainte de sanctions (l'UE a essayé et essuyé plusieurs échecs)

<a href="https://fr.statista.com/infographie/12778/evolution-du-chiffre-affaires-des-gafam/" title="Infographie: L'ascension des GAFAM | Statista"><img src="https://cdn.statcdn.com/Infographic/images/normal/12778.jpeg" alt="Infographie: L'ascension des GAFAM | Statista" width="100%" height="auto" style="width: 100%; height: auto !important; max-width:960px;-ms-interpolation-mode: bicubic;"/></a> Vous trouverez plus d'infographie sur <a href="https://fr.statista.com/graphique-du-jour/">Statista</a>

#### Domination culturelle


- diffusion d'une certaine vision du monde très technophile, issue des hippies californiens ayant pris des virages libertariens ou transhumanistes

    Ces entreprises nous imposent une vision du monde, le fantasme d’un univers connecté et ouvert, une utopie née avec les prémices d’Internet dès les années 1970. Les patrons des géants du web se positionnent, depuis des années, en prophètes d’un monde meilleur, dont leurs systèmes sont censés accoucher. En réalisant leur rêve de « village global numérique », ils standardisent nos consommations, ils formatent nos relations, ils contrôlent nos moyens d’expression. Le projet « progressiste » de ces entreprises est de vous rendre libre, de vous émanciper de gré ou de force.

- censure des contenus, règles tendent à s'uniformiser sur le modèle des USA

- économie de l'attention, capter notre attention pour la publicité. Destruction de notre attention et du lien aux autres

    > « Dans un monde riche en informations, l'abondance d'informations entraîne la pénurie d'une autre ressource : la rareté devient ce que consomme l'information. Ce que l'information consomme est assez évident : c'est l'attention de ses receveurs. Donc une abondance d'informations crée une rareté de l'attention et le besoin de répartir efficacement cette attention parmi la surabondance des sources d'informations qui peuvent la consommer » – *Angie Gaudion*

    Scroll infini, notifications, « feeds » de nouvelles. Dans un monde saturé de messages et de stimulis, ces dispositifs sont autant de stratégies pour capter l’attention des utilisateurs. L’enjeu est de taille : il faut 0,5 seconde pour que le cerveau se déconnecte de ce qu’il est en train de faire pour aller sur une autre sollicitation et décider d’y rester ou pas.

- social credit system en Chine par exemple

#### Collecte des données personnelles

- publicité et marketing ciblée
- manipulation d'opinion

#### Domination politique

S'invitent dans des domaines considérés comme régaliens (éducation: accords ministère et Microsoft, santé : Healt data hub, armée : Microsoft open bar)

Scandale Cambridge Analytics, qui en collectant les données de milliers de profils Facebook ont permis d'établir des profils précis, et orienter la campagne pour le Brexit ou pour Trump.

Pouvoir de filtrage et de censure, les géants du web nourrissent les opinions politiques des internautes.

> Ils « ont un pouvoir politique très important qui est lié à leur situation sur le marché de l'information, ce qui leur permet de trier les informations, de rendre visibles certaines informations et invisibles d'autres » <br>– *Romain Badouard, professeur en sciences de l'information et de la communication*

### Les États et le numérique, surveiller et contrôler

- multiplication et recoupement des fichiers (déjà avant le numérique)
- surveillance des réseaux "deep packet inspection" utilisée
- censures administratives
- des états peuvent demander aux GAFAM de bloquer certains comptes
- crédit social (Chine)

### Capitalisme de surveillance : surveiller et prédire

> Le « capitalisme de surveillance » est le processus qui transforme nos comportements présents en prédictions monnayées de nos comportements futurs.
> *— Sébatien Broca - https://laviedesidees.fr/Surveiller-et-predire.html (mars 2019)*

### Uberisation / plateformisation de la société

L'hégémonie des entreprises et leur position quasi-monopolistique se fait également en exploitant les travailleurs dans le monde entier :

- mineurs de métaux rares
- fabriquants d'équipements électroniques
- nettoyeurs du web qui modèrent les contenus des plateformes Facebook ou Youtube

Certains en ont fait leur modèle économique. Amazon exploitent les livreurs de leurs produits dans le monde entier, dans les entrepôts en imposant des cadences et une robotisation du travail, ou en les faisant livrer les produits via leur plateforme. Deliveroo et Uber Eats imposent des tarifs en baisse régulières aux livreurs supposés être "freelances", qui peuvent être déréférencés s'ils ne travaillent pas assez souvent aux horaires où il y a le plus de demandes. Des travailleurs sont payés à la micro-tâche via les plateformes de travail comme Amazon Mechanical Turk.

Travailleurs du click, nettoyeurs du web, atomisation des collectifs de travail, surveillance, lean management, plateformisation du marché du travail, du logement, de la livraison…, micro-trading, des conséquences sur le travail

### Impacts sociaux, inégalités et haine en ligne

Accès inégal au numérique et perte d'autonomie, automatisation des esprits, surveillance d'état et des entreprises, cybercriminalités, cyberharcèlement, capitalisme de surveillance, atteintes à la vie privée, collectes de données personnelles, 

### Conséquences écologiques

Au niveau mondial: 

→ Consommation d’énergie primaire (EP) : 4,2 %
→ Émissions de gaz à effet de serre (GES) : 3,8 %
→ Consommation d’eau (eau) : 0,2 %

2 à 3 fois l'empreinte de la France

#### Fabrication

- extraction des terres rares, il resterait 30 ans de ressources
- exploitation des travailleurs dans les mines et dans les usines

Dès lors qu'un équipement est fabriqué, il a un impact bien supérieur à tout l'usage qui en est fait.

#### Utilisation

- Consommation d'énergie, 4% de l'énergie mondiale consommée.
Ce qui consomme le plus : les vidéos.

#### Fin de vie des matériels

- Recyclage quasi-inexistant, consommation de masse, obsolescance programmée
- Course à la croissance : plus vite, plus puissants, plus d'équipements connectés,… ex: la 5G

Le plus polluant :
- dans le monde : les mobiles et les objets connectés ?
- en France : les TV et les box internet

## Les aspects positifs

### Partage des connaissances

« Le savoir est un arme »

- accès à de nombreuses connaissances et cultures diverses
- partage des savoirs accru (licences libres, biens non rivaux)
- réappropriation nombreuses (culture du partage, du remix, du mème)
- importance stratégique des données ouvertes (OSM, statistiques,…)

### Utilité dans nos luttes

- accès à l'information plus facile
- horizontalité des échanges
- organisation des collectifs à plus grande distance
- émancipation de certaines minorités qui ont pu faire connaître leurs combats par le biais d'internet (Féminismes, LGBTQI+, Rojava, Exarchaia, Indiens du Dakota du Nord, NDDL)

### Mais toujours porter attention

- à la confidentialité et au respect de la vie privée
- à sourcer et vérifier vos informations
- à ce que les plus à l'aise avec le numérique ne soient pas les seuls à s'exprimer (souvent les hommes)
- à la dispersion et à la multiplication des outils
- à ne pas se contenter d'agir en ligne
- à être autonome vis à vis des plateformes (medium vs votre propre blog hébergé par vos soins cf article activiste iranien critiquant medium, facebook vs votre propre site)

## Quelle éthique ? déconnexion totale ? frugalité ? émancipation ?

- Mieux connaître le numérique pour comprendre son impact
- Chercher à se déconnecter de la machine (cf Félix Tréguer ou le groupe Marcuse)
- Trouver et construire des outils respecteux de nos vie
- Lutter contre les causes des impacts négatifs (le capitalisme ?)

## Ressources

### Articles



### Vidéos

### MOOC

- [MOOC CHATONS module 1 : Internet, reprendre le contrôle](https://mooc.chatons.org/course/view.php?id=3)

### Une sélection de Livres

- A.  Casilli, Antonio. 2019. En attendant les robots. Enquête sur le travail du clic. Éditions du Seuil.
- Bihouix, Philippe. 2014. L’âge des low tech :  vers une civilisation techniquement soutenable. Éditions du Seuil.
- Bollier, David. 2014. La renaissance des communs : pour une société de coopération et de partage. C. L. Mayer.
- Bordage, Frédéric. 2019a. Éco-conception web :  les 115 bonnes pratiques   doper son site et réduire son - empreinte écologique. 3e éd. Paris: Éditions Eyrolles.
- Bordage, Frédéric. 2019b. Sobriété numérique :  les clés pour agir. Paris: Buchet-Chastel.
- Boullier, Dominique. 2019. Sociologie du numérique. 2e édition. Malakoff: Armand Colin.
- Cardon, Dominique. 2019. Culture numérique. Presses de Sciences Po. Presses de Sciences Po.
- Collectif. 2017. Guide d’autodéfense numérique. Éditions Tahin Party.
- Cordier, Anne. 2015. Grandir connectés: les adolescents et la recherche d’information. C & F éditions.
- Desmurget, Michel. 2019. La Fabrique du crétin digital. Éditions du Seuil.
- Ertzscheid, Olivier. 2017. L’appétit des géants: pouvoir des algorithmes, ambitions des plateformes. C & F éditions.
- Groupe Marcuse. 2019. La liberté dans le coma. Essai sur l’identification électronique et les motifs de s’y opposer. 2e édition. La Lenteur éditions.
- Guiton, Amaelle. 2013. Hackers: au coeur de la résistance numérique. Au diable Vauvert.
- Pitron, Guillaume. 2018. La guerre des métaux rares: la face cachée de la transition énergétique et numérique. Paris: Éditions Les Liens qui Libèrent.
- Smyrnaios, Nikos. 2017. Les GAFAM contre l’internet :  une économie politique du numérique. INA.
- Treguer, Félix. 2019. L’utopie déchue: une contre-histoire d’Internet, XVe-XXIe siècle. Paris: Fayard.
- Tufekci, Zeynep. 2019. Twitter et les gaz lacrymogènes. Forces et fragilités de la contestation connectée. C&F éditions. C & F éditions.
- Vion-Dury Philippe. 2016. La nouvelle servitude volontaire: enquête sur le projet politique de la Silicon Valley. Limoges: Fyp éditions.

## Animation possible

(non mobilisée quand j'ai fait cette présentation)

Nombre de participant : 20 à 40
Temps : 2h30, prévoir une pause
Matériel : Rétroprojecteur non obligatoire

### Question en introduction

Internet, le numérique, c'est quoi pour vous ?

### Présentation

- affichage des points abordés sous forme de carte mentale
- matériel : tableau liège, punaises, fil à laine, images imprimées

### Suite en atelier / discussion

- si peu nombreux : choisir 3 sujets
- si 8 < nb < 15 : choisir les sujets en votant par points, parmis 10 ou 12 cartes, à discuter dans l'ordre des points obtenus
- si nb > 15 : découpage en groupes de discussion par thématique, arpentage d'articles choisis, réflexions sur la déconnexion ? trouver les personnes qui ont des connaissances sur un des sujets ou proposer une question à débattre. Mettre à dispo livres, articles imprimés, images…

#### Les sujets

- cartes concepts à approfondir
- cartes questions / débat
- articles à arpenter
